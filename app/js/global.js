"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var page_builder_codeview = function page_builder_codeview(b, page_builder) {

  var my_class = this;

  this.showCode = function (block) {

    b.append('<div class="pb_codeview"><button class="close">Schließen</button><button class="save">Speichern</button></div>');

    //b.find(".pb_codeview #pb_code_editor").html( block.html() );

    if (typeof ace != "undefined") {

      b.find(".pb_codeview").prepend('<div id="pb_code_editor"></div>');
      my_class.editor = ace.edit("pb_code_editor");
      my_class.editor.setValue(block.html());
      my_class.editor.setOptions({
        autoScrollEditorIntoView: true,
        copyWithEmptySelection: true
      });
      my_class.editor.setTheme("ace/theme/twilight");
      my_class.editor.session.setMode("ace/mode/html");
    } else {
      b.find(".pb_codeview").prepend("<textarea></textarea>");
      b.find(".pb_codeview textarea").val(block.html());
    }

    my_class.setCloseButton();
    my_class.setSaveButton(block);
  };

  this.showEmbedCode = function (btn) {

    b.append('<div class="pb_codeview"><div class="pb_embedcode"></div><button class="close">Schließen</button></div>');

    $.ajax({
      type: "POST",
      url: page_builder.settings.embedCode[0],
      data: page_builder.settings.embedCode[1],
      success: function success(ret) {

        my_class.getHandleEmbedCode(ret, btn);
      },

      error: function error(ret) {
        console.log(ret);
      },
      dataType: "json"
    });

    my_class.setCloseButton();
  };

  this.getHandleEmbedCode = function (ret, btn) {

    var html = "";
    $.each(ret, function (k, v) {

      html += '<div class="pb_embedcode_item">' + k + '</div>';
    });

    b.find(".pb_codeview .pb_embedcode").html(html);

    b.find(".pb_codeview .pb_embedcode .pb_embedcode_item").unbind("click");

    b.find(".pb_codeview .pb_embedcode .pb_embedcode_item").click(function () {

      var pb_id = ".pb_code#" + btn.attr("href").replace("#", "").trim();

      b.find(pb_id).html(ret[$(this).text().trim()].html);
      b.find(pb_id).attr("data-code", $(this).text().trim());

      if (typeof page_builder.embed_codes == "undefined") page_builder.embed_codes = {};

      page_builder.embed_codes[pb_id] = b.find(pb_id).html();

      if (ret[$(this).text().trim()].js && ret[$(this).text().trim()].js.length > 0) {

        $.each(ret[$(this).text().trim()].js, function (k, v) {

          //if( $( "script[src='"+v+"']" ).length )
          //return true;

          b.find(pb_id).append('<script src="' + v + '"></script>');

          page_builder.embed_codes[pb_id] = b.find(pb_id).html();
        });
      }

      b.find(".pb_codeview").remove();
    });
  };

  this.showSuccessMsg = function (msg) {

    b.append('<div class="pb_codeview"><div class="pb_save">' + msg + '</div><button class="close">Schließen</button></div>');

    my_class.setCloseButton();
  };

  this.showSave = function (callback) {

    b.append('<div class="pb_codeview"><div class="pb_save"></div><button class="close">Schließen</button><button class="save">Speichern</button></div>');

    var html = '';
    var input = "text";
    $.each(page_builder.settings.document, function (k, v) {

      input = "text";

      if (k == "preview_url" || k == "date") input = "hidden";

      if (input == "text") html += '<label for="' + k + '">' + k + '</label>';

      html += '<input id="' + k + '" type="' + input + '" name="' + k + '" value="' + v + '">';
    });

    html += '<button class="clear">Lösche Bühne</button>';

    if (typeof page_builder.settings.delete_url != "undefined") html += '<button class="clear page">Lösche Seite</button>';

    b.find(".pb_codeview .pb_save").append(html);

    my_class.setCloseButton();

    b.find(".pb_codeview button.clear").unbind("click");
    b.find(".pb_codeview button.clear").click(function () {
      localStorage.setItem("pb_temp", "");
      b.find(".pb_stage").html("");
      b.find(".pb_codeview").remove();
    });

    b.find(".pb_codeview button.clear.page").unbind("click");
    b.find(".pb_codeview button.clear.page").click(function () {
      my_class.getClearPage();
      return false;
    });

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function () {

      b.find(".pb_codeview .pb_save input").each(function () {

        page_builder.settings.document[$(this).attr("name")] = $(this).val();
        b.find(".pb_codeview").remove();
      });

      callback();
    });
    //my_class.setSaveButton(block);
  };

  this.getClearPage = function () {

    b.find(".pb_codeview").remove();

    b.append('<div class="pb_codeview"><div class="pb_clear_page"></div><button class="close">Schließen</button></div>');

    var html = "";
    html += "<h1>" + page_builder.settings.document.title + "</h1>";
    html += "<button class='delete' style='margin-right: 20px;'>Nur diese eine Revision löschen</button>";
    html += "<button class='delete all'>Gesamte Revisionen der Seite löschen</button>";

    var send = {
      document: page_builder.settings.document,
      format: "one"
    };

    b.find(".pb_codeview .pb_clear_page").append(html);

    b.find(".pb_codeview .pb_clear_page button.delete").unbind("click");

    b.find(".pb_codeview .pb_clear_page button.delete").click(function () {

      var btn = $(this);

      if (btn.hasClass("all")) send.format = "all";

      $.ajax({
        type: "POST",
        url: page_builder.settings.delete_url,
        data: send,
        success: function success(ret) {

          page_builder.document = { "title": "Demo", "language": "de", "author": "undefined" };

          b.find(".pb_codeview").remove();
          page_builder.settings.callback_after_save(ret, b, page_builder);
        },

        error: function error(ret) {
          console.log(ret);
        },
        dataType: "json"
      });
    });

    my_class.setCloseButton();
  };

  this.showHref = function (src, callback) {

    b.append('<div class="pb_codeview"><div class="pb_href"></div><button class="close">Schließen</button><button class="save">Speichern</button></div>');

    var html = '';
    html += '<label for="href">Link-URL (http://www...)</label><input id="href" type="text" name="href">';
    html += '<label for="description">Link-Text</label><input type="text" name="description" id="description">';
    html += '<label for="classes">Style-Klassen</label><input type="text" name="classes" id="classes">';
    html += '<label for="target">Neues Fenster?</label>';
    html += '<select name="target">';
    html += '<option value="">Nein</option>';
    html += '<option value="_blank">Ja</option>';
    html += '</select>';

    b.find(".pb_codeview .pb_href").append(html);

    b.find(".pb_codeview .pb_href input[name=href]").val(src.attr("href"));
    b.find(".pb_codeview .pb_href input[name=description]").val(src.text());
    b.find(".pb_codeview .pb_href input[name=classes]").val(src.attr("class"));
    b.find(".pb_codeview .pb_href select[name=target]").val(src.attr("target"));

    my_class.getSelectHref(src, callback);
    my_class.setCloseButton();
  };

  this.showText = function (src, callback) {

    b.append('<div class="pb_codeview"><div class="pb_text"></div><button class="close">Schließen</button><button class="save">Speichern</button></div>');

    var html = '';
    html += '<textarea class="pb_tinymce">' + src.html() + '</textarea>';

    b.find(".pb_codeview .pb_text").append(html);

    b.find(".pb_codeview .pb_text textarea").tinymce({
      script_url: 'https://cdn.hit-or-shit.com/tools/tinymce/tinymce.min.js',
      language: 'en',
      height: '200px',
      mode: "exact",
      relative_urls: false,
      remove_script_host: false,
      theme: "modern",
      skin: "lightgray",
      menubar: '',
      statusbar: true,
      force_br_newlines: true,
      force_p_newlines: false,
      forced_root_block: '',
      toolbar: ["styleselect insert | forecolor backcolor | fontselect fontsizeselect | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | blockquote removeformat | link image mybutton media | undo redo"],

      plugins: ['paste', 'advlist autolink lists link image charmap print preview anchor', 'searchreplace visualblocks code fullscreen', 'insertdatetime media table contextmenu paste code', 'textcolor colorpicker', 'wordcount', 'media'],
      content_css: [page_builder.settings.tinycss],

      paste_auto_cleanup_on_paste: true,
      paste_postprocess: function paste_postprocess(pl, o) {
        o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;+/ig, " ");
      },
      setup: function setup(editor) {
        editor.addButton('mybutton', {
          text: '',
          tooltip: "Image from Galery",
          //icon: 'mce-ico mce-i-nonbreaking',
          icon: 'flix-glyphicon flix-Logo-Terminflix_icon_sw',
          onclick: function onclick() {

            $.get(page_builder.settings.imgs, function (data) {

              my_class.showTinymceImgs(data, b.find(".pb_codeview .pb_text textarea"), function (url) {
                editor.insertContent('<br><br><img src="' + url + '" style="width: 50%;"><br><br>');
              });
            });
          }

        });
      } });

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function () {

      src.html(b.find(".pb_codeview .pb_text textarea").tinymce().getContent());

      b.find(".pb_codeview").remove();
    });

    my_class.setCloseButton();
  };

  this.getSelectHref = function (src, callback) {

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function () {

      src.attr("href", b.find(".pb_codeview .pb_href input[name=href]").val());
      src.text(b.find(".pb_codeview .pb_href input[name=description]").val());
      src.attr("class", b.find(".pb_codeview .pb_href input[name=classes]").val());
      src.attr("target", b.find(".pb_codeview .pb_href select[name=target]").val());

      b.find(".pb_codeview").remove();

      return false;
    });
  };

  this.showIcons = function (data, src, callback) {

    b.append('<div class="pb_codeview pb_overflow"><div class="pb_icons"></div><button class="close">Schließen</button></div>');

    $.each(data, function (k, v) {

      b.find(".pb_codeview .pb_icons").append('<a href="#"><i class="' + v.class + ' ' + v.subclass + '"></i></a>');
    });
    my_class.getSelectIcon(src, callback);
    my_class.setCloseButton();
  };

  this.getSelectIcon = function (src, callback) {

    b.find(".pb_codeview .pb_icons a").unbind("click").click(function () {

      var classes = src.attr("class");
      classes = classes.split(" ");
      var new_src = $(this).find("i");

      var old_classes = "";
      $.each(classes, function (k, v) {

        if (v != v.replace("flix-icon", "")) return true;

        if (v != v.replace("flix-glyphicon", "")) return true;

        new_src.addClass(v);
      });

      src.replaceWith(new_src);

      b.find(".pb_codeview").remove();

      callback(new_src);

      return false;
    });
  };

  this.showIframe = function (iframe) {

    b.append('<div class="pb_codeview pb_overflow"><div class="pb_iframe"></div><button class="close">Schließen</button><button class="save">Speichern</button></div>');

    var html = '';
    html += '<label for="iframe">URL</label><input type="text" name="iframe" id="iframe" value="' + iframe.attr("src") + '">';

    b.find(".pb_codeview .pb_iframe").append(html);

    my_class.setCloseButton();
    my_class.getSelectIframe(iframe);
  };

  this.getSelectIframe = function (iframe) {

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function () {

      iframe.attr("src", my_class.getCheckYoutubeUrl(b.find(".pb_codeview .pb_iframe input[name=iframe]").val()));
      b.find(".pb_codeview").remove();

      return false;
    });
  };

  this.getCheckYoutubeUrl = function (url) {

    if (url.indexOf("/embed/") != -1) return url;

    var new_url = my_class.getChangeYoutubeUrl(url);

    if (!new_url) return url;

    return 'https://www.youtube.com/embed/' + new_url + '?controls=0&showinfo=0&modestbranding=1&rel=0';
  };

  this.getChangeYoutubeUrl = function (url) {

    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
      return match[2];
    } else {
      return false;
    }
  };

  this.showImgs = function (data, src, callback) {

    my_class.getImgs(data);
    my_class.getSelectImg(src, callback);
    my_class.setCloseButton();
  };

  this.showTinymceImgs = function (data, src, callback) {

    my_class.getImgs(data);
    my_class.setCloseButton();
    my_class.getSelectTinymceImg(src, callback);
  };

  this.getSelectTinymceImg = function (src, callback) {

    b.find(".pb_codeview .pb_imgs a").unbind("click").click(function () {

      var new_src = $(this).attr("href");

      $(this).closest(".pb_codeview").remove();

      callback(new_src);

      return false;
    });
  };

  this.getImgs = function (data) {

    b.append('<div class="pb_codeview pb_overflow"><div class="pb_imgs"></div><button class="close">Schließen</button></div>');

    if (typeof data.settings != "undefined") {

      my_class.showImgsTags(data.settings);
      delete data.settings;
    }

    $.each(data, function (k, v) {

      if (typeof v.tag == "undefined") v.tag = "";

      if (typeof v.thumb != "undefined") b.find(".pb_codeview .pb_imgs").append('<a href="' + v.src + '" data-tag="' + v.tag + '"><img src="' + v.thumb + '"></a>');else b.find(".pb_codeview .pb_imgs").append('<a href="' + v.src + '" data-tag="' + v.tag + '"><img src="' + v.src + '"></a>');
    });
  };

  this.showImgsTags = function (settings) {

    b.find(".pb_codeview .pb_imgs").append('<button data-filter-tag="" class="pb_active">* (' + settings.count + ')</button>');

    $.each(settings.tags, function (k, v) {

      b.find(".pb_codeview .pb_imgs").append('<button data-filter-tag="' + k + '">' + k + ' (' + v + ')</button>');
    });

    b.find(".pb_codeview .pb_imgs button").unbind("click");

    b.find(".pb_codeview .pb_imgs button").click(function () {

      b.find(".pb_codeview .pb_imgs button").removeClass("pb_active");

      $(this).addClass("pb_active");

      var filter = $(this).attr("data-filter-tag");

      if (!filter) b.find(".pb_codeview .pb_imgs [data-tag]").show();else {
        b.find(".pb_codeview .pb_imgs [data-tag]").hide();
        b.find(".pb_codeview .pb_imgs [data-tag='" + filter + "']").show();
      }

      return false;
    });
  };

  this.getSelectImg = function (src, callback) {

    b.find(".pb_codeview .pb_imgs a").unbind("click").click(function () {

      var new_src = $(this).attr("href");

      if (src == false) b.find("li[contenteditable].pb_active_panel").append('<img src="' + new_src + '" class="flix-img-responsive">');else src.attr("src", new_src);

      b.find(".pb_codeview").remove();

      callback(new_src);

      return false;
    });
  };

  this.setCloseButton = function () {

    b.find(".pb_codeview button.close").unbind("click");
    b.find(".pb_codeview button.close").click(function () {

      b.find(".pb_codeview").remove();

      return false;
    });
  };

  this.showMenuItem = function (item, callback) {

    b.append('<div class="pb_codeview pb_overflow"><div class="pb_menu_item"></div><button class="close">Schließen</button><button class="save">Hinzufügen</button></div></div>');

    var html = '';

    html += '<select>';

    var val1 = page_builder.settings.menu[b.find(".pb_lang_select").val()][0];

    $.each(page_builder.settings.menu[b.find(".pb_lang_select").val()], function (k, v) {

      html += '<option value="' + v + '">' + v + '</option>';
    });

    html += '</select>';

    html += '<input type="text" value="' + val1 + '">';

    b.find(".pb_codeview .pb_menu_item").append(html);

    if ((typeof item === "undefined" ? "undefined" : _typeof(item)) == "object") {

      if (typeof item.attr("data-menu") == "string") b.find(".pb_codeview .pb_menu_item select").val(item.attr("data-menu"));

      b.find(".pb_codeview .pb_menu_item input").val(item.find(".pb_title").eq(0).text().trim());
      b.find(".pb_codeview .save").text("Ändern");
    }

    b.find(".pb_codeview .pb_menu_item select").change(function () {

      b.find(".pb_codeview .pb_menu_item input").val($(this).val().trim());

      return false;
    });

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function () {

      callback(b.find(".pb_codeview .pb_menu_item select").val(), b.find(".pb_codeview .pb_menu_item input").val());
      b.find(".pb_codeview").remove();

      return false;
    });

    my_class.setCloseButton();
  };

  this.setSaveButton = function (block) {

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function () {

      var code = "";

      if (typeof ace != "undefined") code = my_class.editor.getValue();else code = b.find(".pb_codeview textarea").val();

      block.html(code);

      b.find(".pb_codeview").remove();
      page_builder.setTempSave();

      page_builder.getStageActions();

      return false;
    });
  };
};
'use strict';

var page_builder_editor = function page_builder_editor(b, page_builder) {

  var my_class = this;
  var menu_builder = new page_menu_builder(b, page_builder);

  this.setEditor = function () {

    b.append('<div class="pb_editor pb_hide"></div>');

    b.find(".pb_editor").append('<a href="#" data-command="trash" title="Ausgewähltes Element löschen"><i class="pb_font pb_font-trash"></i></a>');

    if (typeof page_builder.settings.menu_builder != "undefined" && typeof page_builder.settings.menu_builder.load != "undefined") b.find(".pb_editor").append('<a href="#" data-command="menu" title="Menü verwalten">M</a>');

    b.find(".pb_editor").append('<a href="#" data-command="undo" title="Änderungen rückgängig machen"><i class="pb_font pb_font-undo"></i></a>');
    b.find(".pb_editor").append('<a href="#" data-command="redo" title="Änderungen wiederherstellen"><i class="pb_font pb_font-redo"></i></a>');
    my_class.getPreviewMenuIcon();
    //b.find(".pb_editor").append('<a href="#" data-command="h1"><i class="pb_font pb_font-h1"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="h2"><i class="pb_font pb_font-h2"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="h3"><i class="pb_font pb_font-h3"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="h4"><i class="pb_font pb_font-h4"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="p"><i class="pb_font pb_font-p"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="div">div</a>');

    //b.find(".pb_editor").append('<a href="#" data-command="bold"><i class="pb_font pb_font-b"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="italic"><i class="pb_font pb_font-i"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="underline"><i class="pb_font pb_font-u"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="insertimage"><i class="pb_font pb_font-img"></i></a>');

    b.find(".pb_editor").append('<a href="#" data-command="code" title="Quelltext bearbeiten"><i class="pb_font pb_font-code"></i></a>');
    b.find(".pb_editor").append('<a href="#" data-command="move" title="Ausgewähltes Element verschieben"><i class="pb_font pb_font-updown"></i></a>');

    my_class.setEditorMenuTitles();
    my_class.getEditorClick();
    my_class.getEditorReturn();
  };

  this.getPreviewMenuIcon = function () {

    if (typeof page_builder.settings.specific_preview_url == "undefined") return false;

    var link = page_builder.settings.specific_preview_url;

    b.find(".pb_editor").append('<a href="' + link + '" data-command="preview_url" title="Vorschau in neuem Browserfenster"><i class="pb_font pb_font-demo"></i></a>');
  };

  this.setEditorMenuTitles = function () {

    b.find(".pb_editor a").each(function () {

      if ($(this).attr("title").length == 0) $(this).attr("title", $(this).data("command"));
    });
  };

  this.getEditorReturn = function (panel) {

    b.find(".pb_stage").keydown(function (e) {
      if (e.which == 13) {
        document.execCommand('insertHtml', false, '<br>');

        //  b.find(".pb_stage").html( b.find(".pb_stage").html().replace("[br]", "<br />"));


        return false;
      }
    });
  };

  this.getEditorClick = function (panel) {

    b.find(".pb_editor a").unbind("click");

    b.find(".pb_editor a").click(function (e) {

      var command = $(this).data('command');

      switch (command) {

        case "move":
          b.find(".pb_active_panel").removeClass(".pb_active_panel");
          break;

        case "trash":
          b.find(".pb_active_panel").remove();
          break;

        case "code":
          var code = new page_builder_codeview(b, page_builder);
          code.showCode(b.find(".pb_active_panel"));
          break;

        case "preview_url":
          var win = window.open($(this).attr("href"), '_blank');
          win.focus();
          break;

        case "menu":
          menu_builder.start();
          break;

        case "h1":
        case "h2":
        case "h3":
        case "h4":
        case "p":
        case "div":
          document.execCommand('formatBlock', false, command);
          my_class.setShowEditor();
          break;

        case "insertimage":

          $.get(page_builder.settings.imgs, function (data) {

            var code = new page_builder_codeview(b, page_builder);
            code.showImgs(data, false, function (url) {
              b.find("img[src='" + url + "']").addClass("flix-img-responsive flix-img-thumbnail");page_builder.setTempSave();my_class.getEditImage();
            });
          });

          page_builder.setEditImg();
          my_class.setShowEditor();
          break;

        default:
          document.execCommand($(this).data('command'), false, null);
          my_class.setShowEditor();
          break;

      }

      page_builder.setTempSave();

      return false;
    });
  };

  this.getEditImage = function () {

    var img = b.find(".pb_stage li[contenteditable] img");
    img.unbind("click");

    img.click(function () {

      var clicked_img = $(this);

      $.get(page_builder.settings.imgs, function (data) {

        var code = new page_builder_codeview(b, page_builder);
        code.showImgs(data, clicked_img, function () {
          page_builder.setTempSave();
        });
      });
    }).mouseenter(function () {
      $(this).addClass("pb_edit_item");
    }).mouseleave(function () {
      $(this).removeClass("pb_edit_item");
    });
  };

  this.getEditIcon = function () {

    var icon = b.find(".pb_stage li[contenteditable]").find(".flix-glyphicon, .flix-icon");

    icon.unbind("click").click(function () {

      var clicked_img = $(this);

      $.get(page_builder.settings.icons, function (data) {

        var code = new page_builder_codeview(b, page_builder);
        code.showIcons(data, clicked_img, function () {
          page_builder.setTempSave();my_class.getEditIcon();
        });
      });
    }).mouseenter(function () {
      $(this).addClass("pb_edit_item");
    }).mouseleave(function () {
      $(this).removeClass("pb_edit_item");
    });
  };

  this.getEditHref = function () {

    var href = b.find(".pb_stage li[contenteditable]").find("a[href]");

    href.unbind("click").click(function () {

      var clicked_href = $(this);

      var code = new page_builder_codeview(b, page_builder);
      code.showHref(clicked_href, function () {
        page_builder.setTempSave();my_class.getEditIcon();
      });
    }).mouseenter(function () {
      $(this).addClass("pb_edit_item");
    }).mouseleave(function () {
      $(this).removeClass("pb_edit_item");
    });
  };

  this.getEditText = function (textblock) {

    var code = new page_builder_codeview(b, page_builder);
    code.showText(textblock, function () {
      page_builder.setTempSave();
    });
  };

  this.getEditIframe = function () {

    b.find(".pb_stage li[contenteditable] .pb_iframe_edit").remove();

    b.find(".pb_stage li[contenteditable] iframe").each(function () {

      var id = $.now();

      $(this).attr("data-id", id);

      $(this).parent().before("<a href='#" + id + "' class='pb_iframe_edit'>Edit</a>");
    });

    b.find(".pb_stage li[contenteditable] .pb_iframe_edit").unbind("click");
    b.find(".pb_stage li[contenteditable] .pb_iframe_edit").click(function () {

      var id = $(this).attr("href");
      id = id.replace("#", "");

      var iframe = b.find(".pb_stage li[contenteditable] iframe[data-id='" + id + "']");

      var code = new page_builder_codeview(b, page_builder);
      code.showIframe(iframe);

      return false;
    });
  };

  this.getEditorClick2 = function (panel) {

    b.find(".pb_editor a").unbind("click");

    b.find(".pb_editor a").click(function (e) {

      var command = $(this).data('command');

      if (command == 'trash') {
        b.find(".pb_active_panel").remove();
        return false;
      }

      if (command == 'code') {

        var code = new page_builder_codeview(b, page_builder);
        code.showCode(b.find(".pb_active_panel"));
        return false;
      }

      if (command == 'h1' || command == 'h2' || command == 'h3' || command == 'h4' || command == 'p' || command == "div" || command == "") {
        document.execCommand('formatBlock', false, command);
        my_class.setShowEditor();
        return false;
      }
      if (command == 'forecolor' || command == 'backcolor') {
        document.execCommand($(this).data('command'), false, $(this).data('value'));
        my_class.setShowEditor();
        return false;
      }
      if (command == 'createlink' || command == 'insertimage') {
        url = prompt('Enter the link here: ', 'http:\/\/');
        document.execCommand($(this).data('command'), false, url);
        my_class.setShowEditor();
        return false;
      }

      document.execCommand($(this).data('command'), false, null);
      my_class.setShowEditor();

      return false;
    });
  };

  this.getEditCode = function () {

    b.find(".pb_code_edit").remove();

    b.find(".pb_code").each(function () {

      var id = "code_" + $.now();

      $(this).attr("id", id);

      $(this).before("<a href='#" + id + "' class='pb_code_edit'>Inhalt einbetten</a>");
    });

    b.find(".pb_code_edit").unbind("click");
    b.find(".pb_code_edit").click(function () {

      var code = new page_builder_codeview(b, page_builder);
      code.showEmbedCode($(this));
    });
  };

  this.setHideEditor = function () {

    b.find(".pb_editor").addClass("pb_hide").removeClass("pb_show");

    page_builder.startDropping();
  };

  this.setShowEditor = function () {

    b.find(".pb_editor").addClass("pb_show").removeClass("pb_hide");

    page_builder.stopDropping();
    b.find(".pb_menu").removeClass("pb_menu_active");
  };
};
"use strict";

!function () {
  var f,
      c,
      u,
      p,
      d,
      s = [];d = "undefined" != typeof global ? global : window, p = d.jQuery;var v = function v() {
    return d.tinymce;
  };p.fn.tinymce = function (o) {
    var e,
        t,
        i,
        l = this,
        r = "";if (!l.length) return l;if (!o) return v() ? v().get(l[0].id) : null;l.css("visibility", "hidden");var n = function n() {
      var a = [],
          c = 0;u || (m(), u = !0), l.each(function (e, t) {
        var n,
            i = t.id,
            r = o.oninit;i || (t.id = i = v().DOM.uniqueId()), v().get(i) || (n = v().createEditor(i, o), a.push(n), n.on("init", function () {
          var e,
              t = r;l.css("visibility", ""), r && ++c == a.length && ("string" == typeof t && (e = -1 === t.indexOf(".") ? null : v().resolve(t.replace(/\.\w+$/, "")), t = v().resolve(t)), t.apply(e || v(), a));
        }));
      }), p.each(a, function (e, t) {
        t.render();
      });
    };if (d.tinymce || c || !(e = o.script_url)) 1 === c ? s.push(n) : n();else {
      c = 1, t = e.substring(0, e.lastIndexOf("/")), -1 != e.indexOf(".min") && (r = ".min"), d.tinymce = d.tinyMCEPreInit || { base: t, suffix: r }, -1 != e.indexOf("gzip") && (i = o.language || "en", e = e + (/\?/.test(e) ? "&" : "?") + "js=true&core=true&suffix=" + escape(r) + "&themes=" + escape(o.theme || "modern") + "&plugins=" + escape(o.plugins || "") + "&languages=" + (i || ""), d.tinyMCE_GZ || (d.tinyMCE_GZ = { start: function start() {
          var n = function n(e) {
            v().ScriptLoader.markDone(v().baseURI.toAbsolute(e));
          };n("langs/" + i + ".js"), n("themes/" + o.theme + "/theme" + r + ".js"), n("themes/" + o.theme + "/langs/" + i + ".js"), p.each(o.plugins.split(","), function (e, t) {
            t && (n("plugins/" + t + "/plugin" + r + ".js"), n("plugins/" + t + "/langs/" + i + ".js"));
          });
        }, end: function end() {} }));var a = document.createElement("script");a.type = "text/javascript", a.onload = a.onreadystatechange = function (e) {
        e = e || window.event, 2 === c || "load" != e.type && !/complete|loaded/.test(a.readyState) || (v().dom.Event.domLoaded = 1, c = 2, o.script_loaded && o.script_loaded(), n(), p.each(s, function (e, t) {
          t();
        }));
      }, a.src = e, document.body.appendChild(a);
    }return l;
  }, p.extend(p.expr[":"], { tinymce: function tinymce(e) {
      var t;return !!(e.id && "tinymce" in d && (t = v().get(e.id)) && t.editorManager === v());
    } });var m = function m() {
    var r = function r(e) {
      "remove" === e && this.each(function (e, t) {
        var n = l(t);n && n.remove();
      }), this.find("span.mceEditor,div.mceEditor").each(function (e, t) {
        var n = v().get(t.id.replace(/_parent$/, ""));n && n.remove();
      });
    },
        o = function o(i) {
      var e,
          t = this;if (null != i) r.call(t), t.each(function (e, t) {
        var n;(n = v().get(t.id)) && n.setContent(i);
      });else if (0 < t.length && (e = v().get(t[0].id))) return e.getContent();
    },
        l = function l(e) {
      var t = null;return e && e.id && d.tinymce && (t = v().get(e.id)), t;
    },
        u = function u(e) {
      return !!(e && e.length && d.tinymce && e.is(":tinymce"));
    },
        s = {};p.each(["text", "html", "val"], function (e, t) {
      var a = s[t] = p.fn[t],
          c = "text" === t;p.fn[t] = function (e) {
        var t = this;if (!u(t)) return a.apply(t, arguments);if (e !== f) return o.call(t.filter(":tinymce"), e), a.apply(t.not(":tinymce"), arguments), t;var i = "",
            r = arguments;return (c ? t : t.eq(0)).each(function (e, t) {
          var n = l(t);i += n ? c ? n.getContent().replace(/<(?:"[^"]*"|'[^']*'|[^'">])*>/g, "") : n.getContent({ save: !0 }) : a.apply(p(t), r);
        }), i;
      };
    }), p.each(["append", "prepend"], function (e, t) {
      var n = s[t] = p.fn[t],
          r = "prepend" === t;p.fn[t] = function (i) {
        var e = this;return u(e) ? i !== f ? ("string" == typeof i && e.filter(":tinymce").each(function (e, t) {
          var n = l(t);n && n.setContent(r ? i + n.getContent() : n.getContent() + i);
        }), n.apply(e.not(":tinymce"), arguments), e) : void 0 : n.apply(e, arguments);
      };
    }), p.each(["remove", "replaceWith", "replaceAll", "empty"], function (e, t) {
      var n = s[t] = p.fn[t];p.fn[t] = function () {
        return r.call(this, t), n.apply(this, arguments);
      };
    }), s.attr = p.fn.attr, p.fn.attr = function (e, t) {
      var n = this,
          i = arguments;if (!e || "value" !== e || !u(n)) return s.attr.apply(n, i);if (t !== f) return o.call(n.filter(":tinymce"), t), s.attr.apply(n.not(":tinymce"), i), n;var r = n[0],
          a = l(r);return a ? a.getContent({ save: !0 }) : s.attr.apply(p(r), i);
    };
  };
}();
"use strict";

var page_menu_builder = function page_menu_builder(b, page_builder) {

  var my_class = this;
  my_class.once = [];

  var code = new page_builder_codeview(b, page_builder);

  this.start = function () {

    this.getJson();
  };

  this.getMoveHandler = function () {

    b.find(".pb_mover").unbind("click");

    b.find(".pb_mover").click(function () {

      var li = $(this).closest("li");

      if (!li.prev("li").find("ul").length) return false;

      li.prev("li").find("ul").append(li.clone());

      li.remove();

      my_class.getNewOrder();
      my_class.getAddMenuItem();
      my_class.getSaveMenu();

      return false;
    });
  };

  this.getMenuOrder = function (val) {

    my_class.getMoveHandler();

    var drop = new Draggable.Sortable(document.querySelectorAll('.pb_menu_editor ul'), {
      draggable: 'li[data-menu]',
      dropzone: '.pb_menu_editor ul',
      handle: '.pb_handle'

    }).on('drag:stop', function (ev) {

      setTimeout(function () {
        my_class.getNewOrder();
        my_class.getAddMenuItem();
        my_class.getSaveMenu();
      }, 500);
    });
  };

  this.getNewOrder = function () {

    my_class.once = [];
    var val = my_class.getNewVal(b.find(".pb_menu_editor"), {}, false);
    my_class.menu[b.find(".pb_menu_select").val()][b.find(".pb_lang_select").val()] = val;

    b.find(".pb_menu_editor").remove();

    b.find(".pb_lang_select").after('<div class="pb_menu_editor">' + my_class.getMenu(val, "") + '</div>');

    my_class.getMenuOrder(val);
  };

  this.getNewVal = function (row, val, second) {

    row.find("ul li").each(function () {

      var txt = $(this).find(".pb_title").eq(0).text().trim();

      var test = $.inArray(txt, my_class.once);

      if (test != -1) return true;

      if ($(this).find("ul li").length > 0) val[txt] = my_class.getNewVal($(this), {});else val[txt] = $(this).attr("data-menu");

      my_class.once.push(txt);
    });

    return val;
  };

  this.getMenu = function (menu, html, parent) {

    if (typeof menu == "undefined" || !menu) return html;

    html += '<ul>';
    var add_html = "";
    $.each(menu, function (k, v) {

      add_html = "<ul></ul>";

      if (typeof v != "string") add_html = my_class.getMenu(v, "", true);

      html += my_class.getLi(v, k, add_html);
    });

    html += '</ul>';

    return html;
  };

  this.getLi = function (v, k, add_html) {

    if (typeof v != "string") v = "";

    return '<li data-menu="' + v + '"><span class="pb_handle">•••</span><span class="pb_title">' + k + '</span><span class="pb_edit">edit</span><span class="pb_delete"><i class="pb_font pb_font-trash"></i></span><span class="pb_mover"><i class="pb_font pb_font-redo"></i></span> ' + add_html + '</li>';
  };

  this.getSecondSelect = function (key) {

    if (b.find(".pb_lang_select")) b.find(".pb_lang_select").remove();

    if (typeof key == "undefined" || !key) return false;

    var html = "<select class='pb_lang_select'>";

    html += '<option value="">- Bitte auswählen -</option>';

    $.each(my_class.menu[key], function (k, v) {

      html += '<option value="' + k + '">' + k + '</option>';
    });

    html += '</select>';

    b.find(".pb_menu_select").after(html);

    b.find(".pb_lang_select").change(function () {

      b.find(".pb_menu_editor, .pb_add_item").remove();

      if (!$(this).val()) return false;

      b.find(".pb_lang_select").after('<div class="pb_menu_editor">' + my_class.getMenu(my_class.menu[key][$(this).val()], "") + '</div><div class="pb_add_item">+</div>');

      my_class.getMenuOrder(my_class.menu[key][$(this).val()]);

      my_class.getAddMenuItem();
    });
  };

  this.getFirstSelect = function () {

    var html = "<select class='pb_menu_select'>";

    html += '<option value="">- Bitte auswählen -</option>';

    $.each(my_class.menu, function (k, v) {

      html += '<option value="' + k + '">' + k + '</option>';
    });

    html += '</select>';

    b.html('<div class="pb_menu_designer"><a href="#" class="pb_back">zurück</a>' + html + '</div>');

    b.find(".pb_menu_select").change(function () {

      var val = $(this).val();

      my_class.getSecondSelect(val);
    });

    my_class.getClickButtonBack();
  };

  this.getClickButtonBack = function () {

    b.find(".pb_menu_designer .pb_back").unbind("click");

    b.find(".pb_menu_designer .pb_back").click(function () {

      b.html("");

      b.page_manager(page_builder.settings);

      return false;
    });
  };

  this.getRemoveMenuItem = function () {

    b.find(".pb_delete").unbind("click");
    b.find(".pb_delete").click(function () {

      $(this).closest("li").fadeOut(1000, function () {
        $(this).remove();my_class.getNewOrder();
        my_class.getAddMenuItem();
        my_class.getMoveHandler();
        my_class.getSaveMenu();
      });
    });
  };

  this.getAddMenuItem = function () {

    my_class.getRemoveMenuItem();

    b.find(".pb_add_item, .pb_edit").unbind("click");

    b.find(".pb_add_item, .pb_edit").click(function () {

      var item = false;

      if ($(this).hasClass("pb_edit")) item = $(this).closest("li");

      code.showMenuItem(item, function (v, k) {

        if (item) item.replaceWith(my_class.getLi(v, k, '<ul></ul>'));else b.find(".pb_menu_editor ul").eq(0).append(my_class.getLi(v, k, '<ul></ul>'));

        my_class.getNewOrder();
        my_class.getAddMenuItem();
        my_class.getMoveHandler();
        my_class.getSaveMenu();
      });

      return false;
    });
  };

  this.getSaveMenu = function () {

    if (typeof page_builder.settings.menu_builder == "undefined") return false;

    if (typeof page_builder.settings.menu_builder.save == "undefined") return false;

    $.ajax({
      type: "POST",
      url: page_builder.settings.menu_builder.save,
      data: my_class.menu,
      success: function success(ret) {

        console.log(ret);
      },
      dataType: "html"
    });
  };

  this.getJson = function () {

    if (typeof page_builder.settings.menu_builder == "undefined") return false;

    if (typeof page_builder.settings.menu_builder.load == "undefined") return false;

    $(".pb_manager_container").remove();

    $.ajax({
      type: "GET",
      url: page_builder.settings.menu_builder.load,
      data: {},
      success: function success(ret) {
        my_class.menu = ret;
        my_class.getFirstSelect();
      },
      dataType: "json"
    });
  };
};
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var page_builder_menu = function page_builder_menu(b, page_builder, stage) {

  var my_class = this;
  //var stage = new page_builder_stage( b, page_builder );


  this.do = function () {

    my_class.getRightMenu();
  };

  this.getRightMenu = function () {

    b.find(".pb_menu").remove();

    b.append('<div class="pb_menu"><div class="pb_menu_arrow"></div> <div class="pb_menu_area"><ul class="pb_menu_area_blocks pb_drop"></ul></div></div>');

    my_class.getBlocks();
    my_class.getToggleRightMenu();
  };

  this.getClickTag = function () {

    b.find(".pb_tags a").unbind("click");

    b.find(".pb_tags a").click(function () {

      my_class.tag = $(this).text();

      my_class.setToggleTags(my_class.tag);

      return false;
    });

    if (typeof my_class.tag != "undefined") my_class.setToggleTags(my_class.tag);
  };

  this.setToggleTags = function (tag) {

    if (tag == "*") {

      b.find(".pb_menu_area_blocks li[contenteditable]").removeClass("pb_hide");
      return false;
    }

    b.find(".pb_tags").each(function () {

      $(this).closest("li").addClass("pb_hide");

      $(this).find("a").each(function () {

        if ($(this).text() == tag) $(this).closest("li").removeClass("pb_hide");
      });
    });
  };

  this.getBlocks = function () {

    $.get(page_builder.settings.blocks, function (data) {

      b.find(".pb_menu_area_blocks").html("");

      var tags_ = [];

      $.each(data, function (k, v) {

        tags_ = v.tag.split(" ");
        var tags = '<a href="#">*</a>';

        $.each(tags_, function (tk, vk) {

          tags += '<a href="#">' + vk + '</a>';
        });

        b.find(".pb_menu_area_blocks").append('<li title="' + v.name + '" contenteditable><img src="' + page_builder.settings.blocks_img + "/" + v.img + '" data-placeholder ><div class="pb_tags">' + tags + '</div></li>');
      });

      //my_class.stopDropping();
      //my_class.setDropping();
      //my_class.startDropping();

      //alert("blocks ready");

    });
  };

  this.setDropping = function () {

    my_class.stopDropping();

    page_builder.droppable = new Draggable.Sortable(document.querySelectorAll('ul.pb_drop'), {
      draggable: 'li[contenteditable]',
      dropzone: 'ul.pb_drop'
    });

    b.find("li[contenteditable]").removeClass("pb_fixed");
  };

  this.startDropping = function () {

    if (_typeof(page_builder.droppable) != "object") my_class.setDropping();

    var menu = b.find(".pb_menu_area_blocks").html();

    //console.log("timeout");
    my_class.getClickTag();

    page_builder.droppable.on("drag:start", function () {

      my_class.menu_active = false;

      if (b.find(".pb_menu").hasClass("pb_menu_active")) my_class.menu_active = true;

      //if(my_class.menu_active)
      b.find(".pb_menu").removeClass("pb_menu_active");
    }).on('drag:stop', function (ev) {

      setTimeout(function () {
        b.find(".pb_menu_area_blocks").html(menu);

        //console.log("timeout");

        stage.getBlockContent();

        my_class.stopDropping();
      }, 500);
      //if(my_class.menu_active)
      //b.find(".pb_menu").addClass("pb_menu_active");

      b.find(".pb_stage").removeClass("draggable-dropzone--occupied");
    }).on("drag:end", function () {
      my_class.stopDropping();
    });
  };

  this.stopDropping = function () {

    if (typeof page_builder.droppable == "undefined") return false;

    b.find("li[contenteditable]").addClass("pb_fixed");

    page_builder.droppable.removeContainer().removeSensor().off("drag:stop");
    page_builder.droppable.destroy();
    delete page_builder.droppable;
  };

  this.setLoadBlock = function (ev) {};

  this.setDragDestroy = function () {

    my_class.droppable.destroy();
  };

  this.getToggleRightMenu = function () {

    b.find(".pb_menu .pb_menu_arrow").click(function () {

      var menu = $(this).closest(".pb_menu");

      menu.toggleClass("pb_menu_active");

      return false;
    });
  };
};
"use strict";

var page_builder = function page_builder(b, settings) {

  var my_class = this;
  my_class.settings = settings;

  var stage = new page_builder_stage(b, my_class);
  var menu = new page_builder_menu(b, my_class, stage);
  var editor = new page_builder_editor(b, my_class);
  var save_menu = new page_builder_save_menu(b, my_class);

  this.do = function () {

    //console.log(page_builder.droppable);

    stage.do();
    menu.do();
    editor.setEditor();
    save_menu.setSaveMenu();
  };

  this.getStageActions = function () {

    stage.getActions();
  };

  this.startDropping = function () {

    menu.startDropping();
  };

  this.stopDropping = function () {

    menu.stopDropping();
  };

  this.setEditImg = function () {

    stage.getEditImage();
  };

  this.setTempSave = function () {

    var html = "";

    b.find(".pb_stage li[contenteditable]").each(function () {

      html += $(this)[0].outerHTML;
    });

    localStorage.setItem("pb_temp", html);

    //console.log("saved");
  };

  return this;
};

$.fn.page_builder = function (settings) {

  var builder = new page_builder($(this), settings);
  builder.do();
};
"use strict";

var page_builder_manager = function page_builder_manager(b, settings) {

  var my_class = this;

  my_class.settings = settings;

  this.do = function () {

    my_class.get_pages();
  };

  this.getHide = function () {

    var height = b.find(".pb_manager_container").height();

    height = height * -1;
    height += 20;

    b.find(".pb_manager_container").animate({ "top": height + "px" }, 500);
  };

  this.getToggleMenu = function () {

    if (b.find(".pb_manager_menu").hasClass("pb_open")) b.find(".pb_manager_container").animate({ "top": "0px" }, 500);else my_class.getHide();
  };

  this.getMenuButton = function () {

    b.find(".pb_manager_menu").addClass("pb_open");

    b.find(".pb_manager_menu").unbind("click");
    b.find(".pb_manager_menu").click(function () {

      $(this).toggleClass("pb_open");

      my_class.getToggleMenu();

      return false;
    });
  };

  this.getTreeView = function () {

    b.find(".pb_manager li").unbind("click");
    b.find(".pb_manager li").click(function () {

      $(this).find("ul").eq(0).toggleClass("pb_show");
      return false;
    });

    b.find(".pb_manager li ul li").unbind("click");
    b.find(".pb_manager li ul li").click(function () {

      $(this).find("ul").eq(0).toggleClass("pb_show");
      $(this).addClass("pb_show");

      return false;
    });

    b.find(".pb_manager [data-pb-content]").click(function () {

      b.find(".pb_manager").find("li.pb_select").removeClass("pb_select");

      $(this).closest("li").addClass("pb_select").parent("ul").closest("li").addClass("pb_select").parent("ul").closest("li").addClass("pb_select");

      var pb_settings = my_class.settings;

      var send = { "json": $(this).attr("data-pb-content") };

      pb_settings.specific_preview_url = $(this).closest("li").parent("ul").closest("li").find("a").attr("href");

      $.ajax({
        type: "POST",
        url: my_class.settings.documents,
        data: send,
        success: function success(ret) {

          b.find(".pb_manager ul").removeClass("pb_show");
          b.find(".pb_manager_menu").removeClass("pb_open");
          my_class.getToggleMenu();

          pb_settings.document = ret;

          localStorage.setItem("pb_temp", ret.html);
          delete pb_settings.document.html;

          $("#page_builder").html("");
          $("#page_builder").page_builder(pb_settings);
        },

        error: function error(ret) {

          console.log(ret);
        },
        dataType: "json"
      });

      return false;
    });
  };

  this.get_manager = function () {

    b.append('<div class="pb_manager_container"><ul class="pb_manager"></ul><div class="pb_manager_menu"></div></div>');
    settings.menu = {};

    if (!my_class.revisions) {
      my_class.getEmptyPage();
      return false;
    }

    $.each(my_class.revisions, function (k, v) {

      var html = "";
      html += '<li>';
      html += k;
      html += '<ul>';

      $.each(v.revisions, function (k1, v1) {
        html += '<li>';
        html += "<a href='" + v.url[k1] + "' target='_blank'>" + k1 + "</a> (" + v1.length + ")";
        html += '<ul>';

        if (typeof settings.menu[k1] == "undefined") settings.menu[k1] = [];

        settings.menu[k1].push(k);

        $.each(v1, function (k2, v2) {
          html += '<li>';
          html += '<a href="#" data-pb-content="' + v2.json + '">' + v2.date + '</a>';
          html += '</li>';
        });
        html += '</ul>';
        html += '</li>';
      });

      html += '</ul>';
      html += '</li>';

      b.find(".pb_manager").append(html);
    });

    my_class.getTreeView();
    my_class.getMenuButton();
    my_class.getFilterMenu();
  };

  this.getEmptyPage = function () {

    my_class.getMenuButton();
    b.find(".pb_manager_menu").trigger("click");

    $("#page_builder").html("");
    $("#page_builder").page_builder(my_class.settings);
  };

  this.getFilterMenu = function () {

    b.find(".pb_manager_container .pb_manager").prepend("<li class='filter'><input type='text' placeholder='Filter / Schlagwort'></li>");

    b.find(".pb_manager_container .pb_manager .filter input").unbind("keyup");

    b.find(".pb_manager_container .pb_manager .filter input").keyup(function () {

      var filter = $(this).val();

      if (filter.length == 0) {
        b.find(".pb_manager_container .pb_manager li.pb_hide").removeClass("pb_hide");
        return false;
      }

      b.find(".pb_manager_container .pb_manager").children("li").each(function () {

        if ($(this).hasClass("filter")) return true;

        $(this).addClass("pb_hide");

        if ($(this).text().indexOf(filter) != -1) $(this).removeClass("pb_hide");
      });
    });
  };

  this.get_pages = function () {

    $.get(my_class.settings.documents, function (data) {

      my_class.revisions = data;

      my_class.get_manager();
    });
  };

  return this;
};

$.fn.page_manager = function (settings) {

  var builder = new page_builder_manager($(this), settings);
  builder.do();
};
"use strict";

var page_builder_save_menu = function page_builder_save_menu(b, page_builder) {

  var my_class = this;
  var code = new page_builder_codeview(b, page_builder);

  this.setSaveMenu = function () {

    b.append("<a class='pb_save_menu' href='#'></a>");

    b.find(".pb_save_menu").unbind("click");

    b.find(".pb_save_menu").click(function () {

      code.showSave(function () {
        my_class.getPostSave();
      });

      return false;
    });

    this.getPostSave = function () {

      var stage = localStorage.getItem("pb_temp");

      b.append("<div class='pb_virtual_dom'>" + stage + "</div>");

      var send = page_builder.settings.document;

      send.html = "";

      b.find(".pb_virtual_dom li[contenteditable]").each(function () {

        var li = $(this);

        li.find(".pb_iframe_edit").remove();
        li.find(".pb_code_edit").remove();

        if (typeof page_builder.embed_codes != "undefined") {

          if (Object.keys(page_builder.embed_codes).length > 0) {

            $.each(page_builder.embed_codes, function (k, v) {
              li.find(k).html(v);
            });
          }
        }

        send.html += '<section class="pb">' + li.html() + '</section>';
      });

      send.preview_url = page_builder.settings.preview_url;

      b.find(".pb_virtual_dom").remove();

      $.ajax({
        type: "POST",
        url: page_builder.settings.save_url,
        data: send,
        success: function success(ret) {

          delete page_builder.settings.document.html;

          if (typeof page_builder.settings.callback_after_save == "function") page_builder.settings.callback_after_save(ret, b, page_builder);
        },

        error: function error(ret) {
          delete page_builder.settings.document.html;
          console.log(ret);
        },
        dataType: "json"
      });
    };
  };
};
"use strict";

var page_builder_stage = function page_builder_stage(b, page_builder) {

  var my_class = this;
  var editor = new page_builder_editor(b, page_builder);

  this.do = function () {

    my_class.getOpenStage();
    my_class.getActions();
  };

  this.getActions = function () {

    my_class.getMousupListener();
    my_class.getEditContent();
    my_class.getLoadCode();
  };

  this.getOpenStage = function () {

    b.append('<ul class="pb_stage pb_drop"></ul>');

    my_class.getTempStage();
  };

  this.getTempStage = function () {

    //localStorage.setItem( "pb_temp", "" );

    my_class.tempSave = localStorage.getItem("pb_temp");

    if (!my_class.tempSave || my_class.tempSave == null) return false;

    b.find(".pb_stage").append(my_class.tempSave);
  };

  this.getBlockContent = function () {

    b.find(".pb_stage img[data-placeholder]").each(function () {

      var panel = $(this).closest("li");
      var block = panel.attr("title");

      $.get(page_builder.settings.blocks_url + "/" + block + ".html", function (html) {

        panel.html(html);

        my_class.getEditContent();

        page_builder.setTempSave();
      });
    });
  };

  this.getEditImage = function () {

    editor.getEditImage();
    editor.getEditIcon();
    editor.getEditHref();
    editor.getEditIframe();
    editor.getEditCode();
    my_class.getEditText();
  };

  this.getLoadCode = function () {

    if (b.find(".pb_code[data-code]").length == 0) return false;

    $.ajax({
      type: "POST",
      url: page_builder.settings.embedCode[0],
      data: page_builder.settings.embedCode[1],
      success: function success(ret) {

        b.find(".pb_code[data-code]").each(function () {

          var pb_code = $(this);

          pb_code.html(ret[$(this).attr("data-code")].html);

          if (ret[$(this).attr("data-code")].js && ret[$(this).attr("data-code")].js.length > 0) {

            $.each(ret[$(this).attr("data-code")].js, function (k, v) {

              pb_code.append('<script src="' + v + '"></script>');
            });
          }
        });
      },

      error: function error(ret) {
        console.log(ret);
      },
      dataType: "json"
    });
  };

  this.getMousupListener = function () {

    $(document).unbind("mouseup");

    $(document).mouseup(function (e) {

      if (!b.find(".pb_stage li.pb_active_panel").is(e.target) && b.find(".pb_stage li.pb_active_panel").has(e.target).length === 0 && !b.find(".pb_stage li.pb_active_panel").is(":focus")) {
        editor.setHideEditor();
        page_builder.setTempSave();
      }
    });
  };

  this.getEditContent = function () {

    my_class.getEditImage();

    b.find(".pb_stage li[contenteditable]").unbind("click");

    b.find(".pb_stage li[contenteditable]").on("mouseenter", function () {

      $(this).addClass("pb_li_active");
    }).on("mouseleave", function () {

      $(this).removeClass("pb_li_active");
    }).on("click", function () {

      editor.setShowEditor();

      b.find(".pb_stage li[contenteditable]").each(function () {

        $(this).removeClass("pb_active_panel");
      });

      $(this).addClass("pb_active_panel");
    });
  };

  this.getEditText = function () {

    b.find(".pb_stage li[contenteditable] .pb_txt").unbind("click");

    b.find(".pb_stage li[contenteditable] .pb_txt").on('click', function (e) {

      editor.getEditText($(this));

      return false;
    });
  };
};