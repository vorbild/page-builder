var page_builder_stage = function( b, page_builder ){

  var my_class = this;
  var editor = new page_builder_editor( b, page_builder );

  this.do = function(){

    my_class.getOpenStage();
    my_class.getActions();

  };

  this.getActions = function(){

    my_class.getMousupListener();
    my_class.getEditContent();
    my_class.getLoadCode();

  };

  this.getOpenStage = function(){

    b.append( '<ul class="pb_stage pb_drop"></ul>' );

    my_class.getTempStage();

  };

  this.getTempStage = function(){

    //localStorage.setItem( "pb_temp", "" );

    my_class.tempSave = localStorage.getItem("pb_temp");

    if(!my_class.tempSave || my_class.tempSave == null)
      return false;

      b.find(".pb_stage").append( my_class.tempSave );


  };

  this.getBlockContent = function(){

    b.find(".pb_stage img[data-placeholder]").each(function(){

      var panel = $(this).closest("li");
      var block = panel.attr("title");

      $.get( page_builder.settings.blocks_url+"/"+block+".html", function(html){

        panel.html(html);

        my_class.getEditContent();

        page_builder.setTempSave();
      } );


    });


  };

  this.getEditImage = function( ){

    editor.getEditImage();
    editor.getEditIcon();
    editor.getEditHref();
    editor.getEditIframe();
    editor.getEditCode();
    my_class.getEditText();

  };


  this.getLoadCode = function(){

    if( b.find(".pb_code[data-code]").length == 0)
      return false;

      $.ajax({
      type: "POST",
      url: page_builder.settings.embedCode[0],
      data: page_builder.settings.embedCode[1],
      success: function(ret){

        b.find(".pb_code[data-code]").each(function(){

          var pb_code = $(this);

          pb_code.html( ret[$(this).attr("data-code")].html );


          if( ret[$(this).attr("data-code")].js && ret[$(this).attr("data-code")].js.length > 0 ){

            $.each( ret[$(this).attr("data-code")].js, function(k, v){

                pb_code.append( '<script src="'+v+'"></script>' );

            } );
          }

        });


      },

      error: function(ret){
        console.log(ret);
      },
      dataType: "json"
    });


  };

  this.getMousupListener = function(){

    $(document).unbind("mouseup");

    $(document).mouseup(function(e){

    if (!b.find(".pb_stage li.pb_active_panel").is(e.target) && b.find(".pb_stage li.pb_active_panel").has(e.target).length === 0 && !b.find(".pb_stage li.pb_active_panel").is(":focus"))
    {
        editor.setHideEditor();
        page_builder.setTempSave();
    }
    });

  };

  this.getEditContent = function(){

    my_class.getEditImage();


    b.find(".pb_stage li[contenteditable]").unbind("click");


    b.find(".pb_stage li[contenteditable]").on("mouseenter", function(){

      $(this).addClass("pb_li_active");

    }).on("mouseleave", function(){


        $(this).removeClass("pb_li_active");


    }).on("click", function(){

        editor.setShowEditor();

      b.find(".pb_stage li[contenteditable]").each(function(){

          $(this).removeClass("pb_active_panel");

      });

      $(this).addClass("pb_active_panel");




    });





  };

  this.getEditText = function(){


    b.find(".pb_stage li[contenteditable] .pb_txt").unbind("click");

    b.find(".pb_stage li[contenteditable] .pb_txt").on('click', function(e) {

      editor.getEditText( $(this) );

      return false;

    });

  };

};
