var page_builder_editor = function( b, page_builder ){

  var my_class = this;
  var menu_builder = new page_menu_builder(b, page_builder);

  this.setEditor = function(){

    b.append( '<div class="pb_editor pb_hide"></div>');


    b.find(".pb_editor").append('<a href="#" data-command="trash" title="Ausgewähltes Element löschen"><i class="pb_font pb_font-trash"></i></a>');

    if( typeof page_builder.settings.menu_builder != "undefined" && typeof page_builder.settings.menu_builder.load != "undefined" )
      b.find(".pb_editor").append('<a href="#" data-command="menu" title="Menü verwalten">M</a>');

    b.find(".pb_editor").append('<a href="#" data-command="undo" title="Änderungen rückgängig machen"><i class="pb_font pb_font-undo"></i></a>');
    b.find(".pb_editor").append('<a href="#" data-command="redo" title="Änderungen wiederherstellen"><i class="pb_font pb_font-redo"></i></a>');
    my_class.getPreviewMenuIcon();
    //b.find(".pb_editor").append('<a href="#" data-command="h1"><i class="pb_font pb_font-h1"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="h2"><i class="pb_font pb_font-h2"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="h3"><i class="pb_font pb_font-h3"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="h4"><i class="pb_font pb_font-h4"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="p"><i class="pb_font pb_font-p"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="div">div</a>');

    //b.find(".pb_editor").append('<a href="#" data-command="bold"><i class="pb_font pb_font-b"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="italic"><i class="pb_font pb_font-i"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="underline"><i class="pb_font pb_font-u"></i></a>');
    //b.find(".pb_editor").append('<a href="#" data-command="insertimage"><i class="pb_font pb_font-img"></i></a>');

    b.find(".pb_editor").append('<a href="#" data-command="code" title="Quelltext bearbeiten"><i class="pb_font pb_font-code"></i></a>');
    b.find(".pb_editor").append('<a href="#" data-command="move" title="Ausgewähltes Element verschieben"><i class="pb_font pb_font-updown"></i></a>');


    my_class.setEditorMenuTitles();
    my_class.getEditorClick();
    my_class.getEditorReturn();

  };

  this.getPreviewMenuIcon = function(){

    if( typeof page_builder.settings.specific_preview_url == "undefined" )
      return false;

    var link = page_builder.settings.specific_preview_url;

    b.find(".pb_editor").append('<a href="'+link+'" data-command="preview_url" title="Vorschau in neuem Browserfenster"><i class="pb_font pb_font-demo"></i></a>');

  };

  this.setEditorMenuTitles = function(){

    b.find(".pb_editor a").each(function(){

      if($(this).attr("title").length == 0 )
        $(this).attr("title", $(this).data("command"));

    });


  };

  this.getEditorReturn = function( panel ){

    b.find(".pb_stage").keydown(function(e) {
    if(e.which == 13) {
        document.execCommand('insertHtml', false, '<br>');

      //  b.find(".pb_stage").html( b.find(".pb_stage").html().replace("[br]", "<br />"));


        return false;
    }
  });

  };


  this.getEditorClick = function( panel ){

    b.find(".pb_editor a").unbind("click");

    b.find(".pb_editor a").click(function(e){

      var command = $(this).data('command');

      switch(command){

          case "move":
            b.find(".pb_active_panel").removeClass(".pb_active_panel");
          break;

          case "trash":
            b.find(".pb_active_panel").remove();
          break;

          case "code":
            var code = new page_builder_codeview(b, page_builder);
            code.showCode( b.find(".pb_active_panel") );
          break;

          case "preview_url":
            var win = window.open($(this).attr("href"), '_blank');
            win.focus();
          break;

          case "menu":
            menu_builder.start();
          break;

          case "h1":
          case "h2":
          case "h3":
          case "h4":
          case "p":
          case "div":
            document.execCommand('formatBlock', false, command);
            my_class.setShowEditor();
          break;

          case "insertimage":


            $.get(page_builder.settings.imgs, function(data){

              var code = new page_builder_codeview(b, page_builder);
              code.showImgs( data, false, function(url){   b.find("img[src='"+url+"']").addClass("flix-img-responsive flix-img-thumbnail"); page_builder.setTempSave(); my_class.getEditImage(); } );

            });

            page_builder.setEditImg();
            my_class.setShowEditor();
          break;

          default:
          document.execCommand($(this).data('command'), false, null);
          my_class.setShowEditor();
          break;

      }

      page_builder.setTempSave();

      return false;
    });


  };

  this.getEditImage = function( ){


    var img = b.find(".pb_stage li[contenteditable] img");
    img.unbind("click");

    img

    .click(function(){

      var clicked_img = $(this);

      $.get(page_builder.settings.imgs, function(data){

        var code = new page_builder_codeview(b, page_builder);
        code.showImgs( data, clicked_img, function(){  page_builder.setTempSave(); } );

      });

    })

    .mouseenter(function(){ $(this).addClass("pb_edit_item");  })
    .mouseleave( function(){ $(this).removeClass("pb_edit_item");  } );

  };

  this.getEditIcon = function(){

    var icon = b.find(".pb_stage li[contenteditable]").find(".flix-glyphicon, .flix-icon");

    icon
    .unbind("click")
    .click(function(){

      var clicked_img = $(this);

      $.get(page_builder.settings.icons, function(data){

        var code = new page_builder_codeview(b, page_builder);
        code.showIcons( data, clicked_img, function(){  page_builder.setTempSave(); my_class.getEditIcon();  } );

      });

    })

    .mouseenter(function(){ $(this).addClass("pb_edit_item");  })
    .mouseleave( function(){ $(this).removeClass("pb_edit_item");  } );


  };

  this.getEditHref = function(){

    var href = b.find(".pb_stage li[contenteditable]").find("a[href]");

    href
    .unbind("click")
    .click(function(){

      var clicked_href = $(this);

      var code = new page_builder_codeview(b, page_builder);
      code.showHref( clicked_href, function(){  page_builder.setTempSave(); my_class.getEditIcon();  } );

    })

    .mouseenter(function(){ $(this).addClass("pb_edit_item");  })
    .mouseleave( function(){ $(this).removeClass("pb_edit_item");  } );


  };

  this.getEditText = function( textblock ){

    var code = new page_builder_codeview(b, page_builder);
    code.showText( textblock, function(){  page_builder.setTempSave(); } );


  };

  this.getEditIframe = function(){

    b.find(".pb_stage li[contenteditable] .pb_iframe_edit").remove();

    b.find(".pb_stage li[contenteditable] iframe").each(function(){

      var id = $.now();

      $(this).attr("data-id", id);

      $(this).parent().before("<a href='#"+id+"' class='pb_iframe_edit'>Edit</a>");

    });

    b.find(".pb_stage li[contenteditable] .pb_iframe_edit").unbind("click");
    b.find(".pb_stage li[contenteditable] .pb_iframe_edit").click(function(){

      var id = $(this).attr("href");
      id = id.replace("#", "");

      var iframe = b.find(".pb_stage li[contenteditable] iframe[data-id='"+id+"']");

      var code = new page_builder_codeview(b, page_builder);
      code.showIframe( iframe );

      return false;

    });

  };

  this.getEditorClick2 = function( panel ){

    b.find(".pb_editor a").unbind("click");

    b.find(".pb_editor a").click(function(e){

      var command = $(this).data('command');

      if (command == 'trash') {
        b.find(".pb_active_panel").remove();
        return false;
      }

      if (command == 'code') {

        var code = new page_builder_codeview(b, page_builder);
        code.showCode( b.find(".pb_active_panel") );
        return false;
      }

      if (command == 'h1' || command == 'h2' || command == 'h3' || command == 'h4' || command == 'p' || command == "div" || command == "") {
        document.execCommand('formatBlock', false, command);
        my_class.setShowEditor();
        return false;
      }
      if (command == 'forecolor' || command == 'backcolor') {
        document.execCommand($(this).data('command'), false, $(this).data('value'));
        my_class.setShowEditor();
        return false;
      }
      if (command == 'createlink' || command == 'insertimage') {
        url = prompt('Enter the link here: ', 'http:\/\/');
        document.execCommand($(this).data('command'), false, url);
        my_class.setShowEditor();
        return false;
      }

      document.execCommand($(this).data('command'), false, null);
      my_class.setShowEditor();

      return false;
    });

  };

  this.getEditCode = function(){

    b.find(".pb_code_edit").remove();

    b.find(".pb_code").each(function(){

      var id = "code_"+$.now();

      $(this).attr("id", id);

      $(this).before( "<a href='#"+id+"' class='pb_code_edit'>Inhalt einbetten</a>" );

    });

    b.find(".pb_code_edit").unbind("click");
    b.find(".pb_code_edit").click(function(){

      var code = new page_builder_codeview(b, page_builder);
      code.showEmbedCode( $(this) );

    });
  };

  this.setHideEditor = function(){

    b.find(".pb_editor").addClass("pb_hide").removeClass("pb_show");

    page_builder.startDropping();
  };

  this.setShowEditor = function(){

    b.find(".pb_editor").addClass("pb_show").removeClass("pb_hide");

    page_builder.stopDropping();
    b.find(".pb_menu").removeClass("pb_menu_active");

  };

};
