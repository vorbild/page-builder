var page_builder_codeview = function( b, page_builder ){

  var my_class = this;

  this.showCode = function( block ){

    b.append( '<div class="pb_codeview"><button class="close">Schließen</button><button class="save">Speichern</button></div>' );

    //b.find(".pb_codeview #pb_code_editor").html( block.html() );

    if( typeof ace != "undefined" ){

      b.find(".pb_codeview").prepend('<div id="pb_code_editor"></div>');
      my_class.editor = ace.edit("pb_code_editor");
      my_class.editor.setValue(block.html());
      my_class.editor.setOptions({
      autoScrollEditorIntoView: true,
      copyWithEmptySelection: true,
      });
      my_class.editor.setTheme("ace/theme/twilight");
      my_class.editor.session.setMode("ace/mode/html");

    }
    else{
      b.find(".pb_codeview").prepend("<textarea></textarea>");
      b.find(".pb_codeview textarea").val( block.html() );
    }

    my_class.setCloseButton();
    my_class.setSaveButton(block);

  };

  this.showEmbedCode = function( btn ){

    b.append( '<div class="pb_codeview"><div class="pb_embedcode"></div><button class="close">Schließen</button></div>' );

    $.ajax({
    type: "POST",
    url: page_builder.settings.embedCode[0],
    data: page_builder.settings.embedCode[1],
    success: function(ret){

      my_class.getHandleEmbedCode(ret, btn);

    },

    error: function(ret){
      console.log(ret);
    },
    dataType: "json"
  });


    my_class.setCloseButton();


  };

  this.getHandleEmbedCode = function(ret, btn){

    var html = "";
    $.each(ret, function(k, v){

      html += '<div class="pb_embedcode_item">'+k+'</div>';

    });

    b.find(".pb_codeview .pb_embedcode").html(html);

    b.find(".pb_codeview .pb_embedcode .pb_embedcode_item").unbind("click");

    b.find(".pb_codeview .pb_embedcode .pb_embedcode_item").click(function(){

      var pb_id = ".pb_code#"+btn.attr("href").replace("#","").trim();

      b.find(pb_id).html( ret[$(this).text().trim()].html );
      b.find(pb_id).attr( "data-code", $(this).text().trim() );

      if(typeof page_builder.embed_codes == "undefined")
        page_builder.embed_codes = {};

      page_builder.embed_codes[pb_id] = b.find(pb_id).html();

      if( ret[$(this).text().trim()].js && ret[$(this).text().trim()].js.length > 0 ){

        $.each( ret[$(this).text().trim()].js, function(k, v){

          //if( $( "script[src='"+v+"']" ).length )
            //return true;

            b.find(pb_id).append( '<script src="'+v+'"></script>' );

            page_builder.embed_codes[pb_id] = b.find(pb_id).html();

        } );
      }

      b.find(".pb_codeview").remove();


    });

  };

  this.showSuccessMsg = function( msg ){

    b.append( '<div class="pb_codeview"><div class="pb_save">'+msg+'</div><button class="close">Schließen</button></div>' );

    my_class.setCloseButton();

  };

  this.showSave = function( callback ){

    b.append( '<div class="pb_codeview"><div class="pb_save"></div><button class="close">Schließen</button><button class="save">Speichern</button></div>' );

    var html = '';
    var input = "text";
    $.each(  page_builder.settings.document, function(k, v){


      input = "text";

      if( k == "preview_url" || k == "date" )
        input = "hidden";

      if(input == "text")
        html += '<label for="'+k+'">'+k+'</label>';

      html += '<input id="'+k+'" type="'+input+'" name="'+k+'" value="'+v+'">';

    } );

    html += '<button class="clear">Lösche Bühne</button>';

    if(  typeof(page_builder.settings.delete_url) != "undefined" )
      html += '<button class="clear page">Lösche Seite</button>';

    b.find(".pb_codeview .pb_save").append( html );

    my_class.setCloseButton();

    b.find(".pb_codeview button.clear").unbind("click");
    b.find(".pb_codeview button.clear").click(function(){
        localStorage.setItem( "pb_temp", "" );
        b.find(".pb_stage").html("");
        b.find(".pb_codeview").remove();
    });

    b.find(".pb_codeview button.clear.page").unbind("click");
    b.find(".pb_codeview button.clear.page").click(function(){
      my_class.getClearPage();
      return false;
    });

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function(){


        b.find(".pb_codeview .pb_save input").each(function(){

          page_builder.settings.document[ $(this).attr("name") ] = $(this).val();
          b.find(".pb_codeview").remove();

        });

        callback();

    });
    //my_class.setSaveButton(block);
  };

  this.getClearPage = function(){

    b.find(".pb_codeview").remove();

    b.append( '<div class="pb_codeview"><div class="pb_clear_page"></div><button class="close">Schließen</button></div>' );

    var html = "";
    html += "<h1>"+page_builder.settings.document.title+"</h1>";
    html += "<button class='delete' style='margin-right: 20px;'>Nur diese eine Revision löschen</button>";
    html += "<button class='delete all'>Gesamte Revisionen der Seite löschen</button>";

    var send = {
      document: page_builder.settings.document,
      format: "one"
    };

    b.find(".pb_codeview .pb_clear_page").append(html);

    b.find(".pb_codeview .pb_clear_page button.delete").unbind("click");

    b.find(".pb_codeview .pb_clear_page button.delete").click(function(){

      var btn = $(this);

      if(btn.hasClass("all"))
        send.format = "all";


    $.ajax({
    type: "POST",
    url: page_builder.settings.delete_url,
    data: send,
    success: function(ret){


      page_builder.document = {"title": "Demo", "language": "de", "author": "undefined"};

      b.find(".pb_codeview").remove();
      page_builder.settings.callback_after_save(ret, b, page_builder);

    },

    error: function(ret){
      console.log(ret);
    },
    dataType: "json"
  });

});

    my_class.setCloseButton();

  };

  this.showHref = function( src, callback ){


    b.append( '<div class="pb_codeview"><div class="pb_href"></div><button class="close">Schließen</button><button class="save">Speichern</button></div>' );

    var html = '';
    html += '<label for="href">Link-URL (http://www...)</label><input id="href" type="text" name="href">';
    html += '<label for="description">Link-Text</label><input type="text" name="description" id="description">';
    html += '<label for="classes">Style-Klassen</label><input type="text" name="classes" id="classes">';
    html += '<label for="target">Neues Fenster?</label>';
    html += '<select name="target">';
    html += '<option value="">Nein</option>';
    html += '<option value="_blank">Ja</option>';
    html += '</select>';

    b.find(".pb_codeview .pb_href").append( html );

    b.find(".pb_codeview .pb_href input[name=href]").val( src.attr("href") );
    b.find(".pb_codeview .pb_href input[name=description]").val( src.text() );
    b.find(".pb_codeview .pb_href input[name=classes]").val( src.attr("class") );
    b.find(".pb_codeview .pb_href select[name=target]").val( src.attr("target") );

    my_class.getSelectHref(src, callback);
    my_class.setCloseButton();

  };

  this.showText = function( src, callback ){


    b.append( '<div class="pb_codeview"><div class="pb_text"></div><button class="close">Schließen</button><button class="save">Speichern</button></div>' );

    var html = '';
    html += '<textarea class="pb_tinymce">'+src.html()+'</textarea>';


    b.find(".pb_codeview .pb_text").append( html );

    b.find(".pb_codeview .pb_text textarea").tinymce({
      script_url : 'https://cdn.hit-or-shit.com/tools/tinymce/tinymce.min.js',
      language: 'en',
      height: '200px',
      mode : "exact",
      relative_urls: false,
		  remove_script_host: false,
      theme: "modern",
      skin: "lightgray",
      menubar : '',
      statusbar : true,
      force_br_newlines : true,
		  force_p_newlines : false,
		  forced_root_block : '',
        toolbar: [
            "styleselect insert | forecolor backcolor | fontselect fontsizeselect | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | blockquote removeformat | link image mybutton media | undo redo"
        ],

         plugins: ['paste',
		    'advlist autolink lists link image charmap print preview anchor',
		    'searchreplace visualblocks code fullscreen',
		    'insertdatetime media table contextmenu paste code',
		    'textcolor colorpicker',
		    'wordcount',
		    'media'
 			],
      content_css: [page_builder.settings.tinycss],

        paste_auto_cleanup_on_paste : true,
        paste_postprocess : function( pl, o ) {
            o.node.innerHTML = o.node.innerHTML.replace( /&nbsp;+/ig, " " );
        },
        setup: function (editor) {
          editor.addButton('mybutton', {
            text: '',
            tooltip: "Image from Galery",
            //icon: 'mce-ico mce-i-nonbreaking',
            icon: 'flix-glyphicon flix-Logo-Terminflix_icon_sw',
            onclick: function () {

              $.get(page_builder.settings.imgs, function(data){

                my_class.showTinymceImgs( data, b.find(".pb_codeview .pb_text textarea"), function(url){
                  editor.insertContent('<br><br><img src="'+url+'" style="width: 50%;"><br><br>');

                  } );


              });

            }

   }); } });

   b.find(".pb_codeview button.save").unbind("click");
   b.find(".pb_codeview button.save").click(function(){

     src.html( b.find(".pb_codeview .pb_text textarea").tinymce().getContent() );

     b.find(".pb_codeview").remove();

   });

    my_class.setCloseButton();

  };

  this.getSelectHref = function( src, callback ){

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function(){

      src.attr("href", b.find(".pb_codeview .pb_href input[name=href]").val());
      src.text(b.find(".pb_codeview .pb_href input[name=description]").val());
      src.attr("class", b.find(".pb_codeview .pb_href input[name=classes]").val());
      src.attr("target", b.find(".pb_codeview .pb_href select[name=target]").val());

      b.find(".pb_codeview").remove();


      return false;

    });

  };

  this.showIcons = function( data, src, callback ){

    b.append( '<div class="pb_codeview pb_overflow"><div class="pb_icons"></div><button class="close">Schließen</button></div>' );

    $.each(data, function(k, v){

      b.find(".pb_codeview .pb_icons").append( '<a href="#"><i class="'+v.class+' '+v.subclass+'"></i></a>' );

    });
    my_class.getSelectIcon(src, callback);
    my_class.setCloseButton();

  };

  this.getSelectIcon = function(src, callback){

    b.find(".pb_codeview .pb_icons a")
    .unbind("click")
    .click(function(){

      var classes = src.attr("class");
      classes = classes.split(" ");
      var new_src = $(this).find("i");

      var old_classes = "";
      $.each(classes, function(k, v){

          if(v != v.replace("flix-icon", ""))
            return true;

          if(v != v.replace("flix-glyphicon", ""))
            return true;

          new_src.addClass(v);

      });

      src.replaceWith(new_src);

      b.find(".pb_codeview").remove();

      callback(new_src);

      return false;
    });


  };

  this.showIframe = function(iframe){

      b.append( '<div class="pb_codeview pb_overflow"><div class="pb_iframe"></div><button class="close">Schließen</button><button class="save">Speichern</button></div>' );

      var html = '';
      html += '<label for="iframe">URL</label><input type="text" name="iframe" id="iframe" value="'+iframe.attr("src")+'">';


      b.find(".pb_codeview .pb_iframe").append( html );

      my_class.setCloseButton();
      my_class.getSelectIframe(iframe);

  };

  this.getSelectIframe = function(iframe){

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function(){

      iframe.attr("src", my_class.getCheckYoutubeUrl( b.find(".pb_codeview .pb_iframe input[name=iframe]").val() ) );
      b.find(".pb_codeview").remove();

      return false;

    });

  };

  this.getCheckYoutubeUrl = function( url ){

    if( url.indexOf( "/embed/" ) != -1 )
      return url;

    var new_url = my_class.getChangeYoutubeUrl( url );

    if(!new_url)
      return url;

    return 'https://www.youtube.com/embed/'+new_url+'?controls=0&showinfo=0&modestbranding=1&rel=0';

  };

  this.getChangeYoutubeUrl = function( url ){

    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return false;
    }

  };

  this.showImgs = function( data, src, callback ){

    my_class.getImgs(data);
    my_class.getSelectImg(src, callback);
    my_class.setCloseButton();

  };

  this.showTinymceImgs = function(data, src, callback){

    my_class.getImgs(data);
    my_class.setCloseButton();
    my_class.getSelectTinymceImg(src, callback);
  };

  this.getSelectTinymceImg = function(src, callback){

    b.find(".pb_codeview .pb_imgs a")
    .unbind("click")
    .click(function(){

      var new_src = $(this).attr("href");


      $(this).closest(".pb_codeview").remove();

      callback(new_src);

      return false;
    });

  };

  this.getImgs = function(data){

    b.append( '<div class="pb_codeview pb_overflow"><div class="pb_imgs"></div><button class="close">Schließen</button></div>' );

    if( typeof data.settings != "undefined" ){

      my_class.showImgsTags(data.settings);
      delete data.settings;

    }

    $.each(data, function(k, v){

      if( typeof v.tag == "undefined" )
        v.tag = "";

      if( typeof v.thumb != "undefined" )
        b.find(".pb_codeview .pb_imgs").append( '<a href="'+v.src+'" data-tag="'+v.tag+'"><img src="'+v.thumb+'"></a>' );

      else
        b.find(".pb_codeview .pb_imgs").append( '<a href="'+v.src+'" data-tag="'+v.tag+'"><img src="'+v.src+'"></a>' );

    });

  };

  this.showImgsTags = function(settings){

    b.find(".pb_codeview .pb_imgs").append( '<button data-filter-tag="" class="pb_active">* ('+settings.count+')</button>' );

    $.each( settings.tags, function(k, v){

      b.find(".pb_codeview .pb_imgs").append( '<button data-filter-tag="'+k+'">'+k+' ('+v+')</button>' );

    });

    b.find(".pb_codeview .pb_imgs button").unbind("click");

    b.find(".pb_codeview .pb_imgs button").click(function(){

      b.find(".pb_codeview .pb_imgs button").removeClass("pb_active");

      $(this).addClass("pb_active");

      var filter = $(this).attr("data-filter-tag");

      if( !filter )
        b.find(".pb_codeview .pb_imgs [data-tag]").show();
      else{
        b.find(".pb_codeview .pb_imgs [data-tag]").hide();
        b.find(".pb_codeview .pb_imgs [data-tag='"+filter+"']").show();
      }

      return false;

    });

  };

  this.getSelectImg = function(src, callback){

      b.find(".pb_codeview .pb_imgs a")
      .unbind("click")
      .click(function(){

        var new_src = $(this).attr("href");


        if( src == false )
          b.find("li[contenteditable].pb_active_panel").append('<img src="'+new_src+'" class="flix-img-responsive">');
        else
          src.attr("src", new_src);



        b.find(".pb_codeview").remove();

        callback(new_src);

        return false;
      });


  };

  this.setCloseButton = function(){

      b.find(".pb_codeview button.close").unbind("click");
      b.find(".pb_codeview button.close").click(function(){

        b.find(".pb_codeview").remove();

        return false;

      });

  };

  this.showMenuItem = function(item, callback){



    b.append( '<div class="pb_codeview pb_overflow"><div class="pb_menu_item"></div><button class="close">Schließen</button><button class="save">Hinzufügen</button></div></div>' );

    var html = '';

    html += '<select>';

    var val1 = page_builder.settings.menu[ b.find(".pb_lang_select").val()][0];

    $.each( page_builder.settings.menu[ b.find(".pb_lang_select").val() ], function(k, v){

      html += '<option value="'+v+'">'+v+'</option>';

    });

    html += '</select>';

    html += '<input type="text" value="'+val1+'">';

    b.find(".pb_codeview .pb_menu_item").append(html);

    if( typeof item == "object" ){

      if(typeof item.attr("data-menu") == "string")
        b.find(".pb_codeview .pb_menu_item select").val( item.attr("data-menu") );

      b.find(".pb_codeview .pb_menu_item input").val( item.find(".pb_title").eq(0).text().trim() );
      b.find(".pb_codeview .save").text("Ändern");

    }

    b.find(".pb_codeview .pb_menu_item select").change(function(){

      b.find(".pb_codeview .pb_menu_item input").val( $(this).val().trim() );

      return false;
    });

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function(){

      callback( b.find(".pb_codeview .pb_menu_item select").val(), b.find(".pb_codeview .pb_menu_item input").val() );
      b.find(".pb_codeview").remove();

      return false;

    });

    my_class.setCloseButton();

  };

  this.setSaveButton = function( block ){

    b.find(".pb_codeview button.save").unbind("click");
    b.find(".pb_codeview button.save").click(function(){

      var code = "";

      if( typeof ace != "undefined" )
        code = my_class.editor.getValue();

      else
        code = b.find(".pb_codeview textarea").val();


      block.html( code );

      b.find(".pb_codeview").remove();
      page_builder.setTempSave();

      page_builder.getStageActions();

      return false;

    });

  };

};
