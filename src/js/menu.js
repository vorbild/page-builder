var page_builder_menu = function( b, page_builder, stage ){

  var my_class = this;
  //var stage = new page_builder_stage( b, page_builder );



  this.do = function(){

    my_class.getRightMenu();

  };

  this.getRightMenu = function(){

    b.find(".pb_menu").remove();

    b.append( '<div class="pb_menu"><div class="pb_menu_arrow"></div> <div class="pb_menu_area"><ul class="pb_menu_area_blocks pb_drop"></ul></div></div>' );

    my_class.getBlocks();
    my_class.getToggleRightMenu();

  };

  this.getClickTag = function(){

    b.find(".pb_tags a").unbind("click");

    b.find(".pb_tags a").click(function(){

        my_class.tag = $(this).text();

        my_class.setToggleTags(my_class.tag);

        return false;

    });

    if(typeof my_class.tag != "undefined")
      my_class.setToggleTags(my_class.tag);

  };

  this.setToggleTags = function(tag){

    if( tag == "*" ){

      b.find(".pb_menu_area_blocks li[contenteditable]").removeClass("pb_hide");
      return false;
    }

    b.find(".pb_tags").each(function(){

      $(this).closest("li").addClass("pb_hide");

      $(this).find("a").each(function(){

        if( $(this).text() == tag )
          $(this).closest("li").removeClass("pb_hide");

      });

    });

  };

  this.getBlocks = function(){


    $.get( page_builder.settings.blocks, function(data){

      b.find(".pb_menu_area_blocks").html("");

      var tags_ = [];

      $.each( data, function(k, v){

        tags_ = v.tag.split(" ");
        var tags = '<a href="#">*</a>';

        $.each(tags_, function(tk, vk){

          tags += '<a href="#">'+vk+'</a>';

        });

        b.find(".pb_menu_area_blocks").append( '<li title="'+v.name+'" contenteditable><img src="'+page_builder.settings.blocks_img+"/"+v.img+'" data-placeholder ><div class="pb_tags">'+tags+'</div></li>' );

      });



      //my_class.stopDropping();
      //my_class.setDropping();
      //my_class.startDropping();

      //alert("blocks ready");


    });

  };



  this.setDropping = function(){

      my_class.stopDropping();

  page_builder.droppable = new Draggable.Sortable(document.querySelectorAll('ul.pb_drop'), {
     draggable: 'li[contenteditable]',
     dropzone: 'ul.pb_drop'
   });

  b.find("li[contenteditable]").removeClass("pb_fixed");



 };

this.startDropping = function(){


if( typeof page_builder.droppable != "object"  )
  my_class.setDropping();

  var menu = b.find(".pb_menu_area_blocks").html();

  //console.log("timeout");
  my_class.getClickTag();


  page_builder.droppable.on("drag:start", function(){

    my_class.menu_active = false;

  if( b.find(".pb_menu").hasClass("pb_menu_active") )
   my_class.menu_active = true;

    //if(my_class.menu_active)
      b.find(".pb_menu").removeClass("pb_menu_active");

  }).on('drag:stop', function (ev) {


      setTimeout(function(){
        b.find(".pb_menu_area_blocks").html(menu);

        //console.log("timeout");

        stage.getBlockContent();

        my_class.stopDropping();


      }, 500);
     //if(my_class.menu_active)
       //b.find(".pb_menu").addClass("pb_menu_active");

     b.find(".pb_stage").removeClass("draggable-dropzone--occupied");




   }).on("drag:end", function(){
     my_class.stopDropping();


   });

};

 this.stopDropping = function(){

   if( typeof page_builder.droppable == "undefined"  )
    return false;

    b.find("li[contenteditable]").addClass("pb_fixed");

   page_builder.droppable.removeContainer().removeSensor().off("drag:stop");
   page_builder.droppable.destroy();
   delete page_builder.droppable;



 };

 this.setLoadBlock = function(ev){



 };

 this.setDragDestroy = function(){

   my_class.droppable.destroy();

 };

  this.getToggleRightMenu = function(){

    b.find(".pb_menu .pb_menu_arrow").click(function(){

      var menu = $(this).closest(".pb_menu");

      menu.toggleClass("pb_menu_active");

      return false;

    });

  };

};
