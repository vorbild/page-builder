var page_builder = function(b, settings){

  var my_class = this;
  my_class.settings = settings;

  var stage = new page_builder_stage( b, my_class  );
  var menu = new page_builder_menu( b, my_class, stage );
  var editor = new page_builder_editor( b, my_class  );
  var save_menu = new page_builder_save_menu( b, my_class  );


  this.do = function(){

    //console.log(page_builder.droppable);

    stage.do();
    menu.do();
    editor.setEditor();
    save_menu.setSaveMenu();

  };

  this.getStageActions = function(){

    stage.getActions();

  };

  this.startDropping = function(){

      menu.startDropping();
  };

  this.stopDropping = function(){

      menu.stopDropping();
  };

  this.setEditImg = function(){

    stage.getEditImage();

  };

  this.setTempSave = function(){

    var html = "";


    b.find(".pb_stage li[contenteditable]").each(function(){

      html += $(this)[0].outerHTML;

    });

    localStorage.setItem( "pb_temp", html );

    //console.log("saved");
  };

  return this;

};

$.fn.page_builder = function(settings){

  var builder = new page_builder( $(this), settings );
  builder.do();


};
