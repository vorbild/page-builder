var page_builder_manager = function(b, settings){

  var my_class = this;

  my_class.settings = settings;


  this.do = function(){

    my_class.get_pages();

  };

  this.getHide = function(){


    var height = b.find(".pb_manager_container").height();

    height = height * -1;
    height+= 20;

    b.find(".pb_manager_container").animate({"top": height+"px"}, 500);

  };

  this.getToggleMenu = function(){

    if( b.find(".pb_manager_menu").hasClass("pb_open") )
      b.find(".pb_manager_container").animate({"top":"0px"}, 500);

    else
      my_class.getHide();

  };

  this.getMenuButton = function(){

    b.find(".pb_manager_menu").addClass("pb_open");

    b.find(".pb_manager_menu").unbind("click");
    b.find(".pb_manager_menu").click(function(){

      $(this).toggleClass("pb_open");

      my_class.getToggleMenu();

      return false;

    });

  };



  this.getTreeView = function(){



    b.find(".pb_manager li").unbind("click");
    b.find(".pb_manager li").click(function(){

      $(this).find("ul").eq(0).toggleClass("pb_show");
      return false;

    });

    b.find(".pb_manager li ul li").unbind("click");
    b.find(".pb_manager li ul li").click(function(){

      $(this).find("ul").eq(0).toggleClass("pb_show");
      $(this).addClass("pb_show");

      return false;
    });



    b.find(".pb_manager [data-pb-content]").click(function(){


      b.find(".pb_manager").find("li.pb_select").removeClass("pb_select");

      $(this).closest("li").addClass("pb_select").parent("ul").closest("li").addClass("pb_select").parent("ul").closest("li").addClass("pb_select");


      var pb_settings = my_class.settings;

      var send = { "json": $(this).attr("data-pb-content") };

      pb_settings.specific_preview_url = $(this).closest("li").parent("ul").closest("li").find("a").attr("href");


      $.ajax({
      type: "POST",
      url: my_class.settings.documents,
      data: send,
      success: function(ret){

        b.find(".pb_manager ul").removeClass("pb_show");
        b.find(".pb_manager_menu").removeClass("pb_open");
        my_class.getToggleMenu();

        pb_settings.document = ret;



        localStorage.setItem("pb_temp", ret.html);
        delete pb_settings.document.html;


        $("#page_builder").html("");
        $("#page_builder").page_builder(pb_settings);

      },

      error: function(ret){

        console.log(ret);

      },
      dataType: "json"
    });


      return false;

    });

  };

  this.get_manager = function(){

    b.append('<div class="pb_manager_container"><ul class="pb_manager"></ul><div class="pb_manager_menu"></div></div>');
    settings.menu = {};

    if( !my_class.revisions ){
      my_class.getEmptyPage();
      return false;

    }

    $.each(my_class.revisions, function(k, v){

      var html = "";
      html += '<li>';
      html += k;
      html += '<ul>';

      $.each(v.revisions, function(k1, v1){
        html += '<li>';
        html += "<a href='"+v.url[k1]+"' target='_blank'>"+k1+"</a> ("+v1.length+")";
        html += '<ul>';

        if( typeof settings.menu[k1] == "undefined" )
          settings.menu[k1] = [];

        settings.menu[k1].push( k );

        $.each(v1, function(k2, v2){
          html += '<li>';
          html += '<a href="#" data-pb-content="'+v2.json+'">'+v2.date+'</a>';
          html += '</li>';
        });
        html += '</ul>';
        html += '</li>';
      });

      html += '</ul>';
      html += '</li>';

      b.find(".pb_manager").append(html);


    });

    my_class.getTreeView();
    my_class.getMenuButton();
    my_class.getFilterMenu();
  };

  this.getEmptyPage = function(){

    my_class.getMenuButton();
    b.find(".pb_manager_menu").trigger("click");


    $("#page_builder").html("");
    $("#page_builder").page_builder(my_class.settings);


  };

  this.getFilterMenu = function(){

    b.find(".pb_manager_container .pb_manager").prepend("<li class='filter'><input type='text' placeholder='Filter / Schlagwort'></li>");

    b.find(".pb_manager_container .pb_manager .filter input").unbind("keyup");

    b.find(".pb_manager_container .pb_manager .filter input").keyup(function(){

      var filter = $(this).val();

      if(filter.length == 0){
        b.find(".pb_manager_container .pb_manager li.pb_hide").removeClass("pb_hide");
        return false;
      }

        b.find(".pb_manager_container .pb_manager").children("li").each(function(){

          if($(this).hasClass("filter"))
            return true;

          $(this).addClass("pb_hide");

          if( $(this).text().indexOf(filter) != -1 )
            $(this).removeClass("pb_hide");

        });

    });

  };

  this.get_pages = function(){

    $.get( my_class.settings.documents, function(data){



      my_class.revisions = data;


      my_class.get_manager();
    });

  };

  return this;

};

$.fn.page_manager = function(settings){

  var builder = new page_builder_manager( $(this), settings );
  builder.do();



};
