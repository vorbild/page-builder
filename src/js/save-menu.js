var page_builder_save_menu = function( b, page_builder ){

  var my_class = this;
  var code = new page_builder_codeview(b, page_builder);

  this.setSaveMenu = function( ){

    b.append( "<a class='pb_save_menu' href='#'></a>" );

    b.find(".pb_save_menu").unbind("click");

    b.find(".pb_save_menu").click(function(){

      code.showSave( function(){ my_class.getPostSave(); } );


      return false;

    });


    this.getPostSave = function(){

      var stage = localStorage.getItem("pb_temp");

      b.append("<div class='pb_virtual_dom'>"+stage+"</div>");

      var send = page_builder.settings.document;

      send.html = "";

      b.find(".pb_virtual_dom li[contenteditable]").each(function(){

        var li = $(this);

        li.find(".pb_iframe_edit").remove();
        li.find(".pb_code_edit").remove();


        if( typeof page_builder.embed_codes != "undefined" ){

          if(Object.keys(page_builder.embed_codes).length > 0 ){

           $.each(page_builder.embed_codes, function(k, v){
              li.find(k).html(v);
            });

          }

        }

        send.html += '<section class="pb">'+li.html()+'</section>';

      });

      send.preview_url = page_builder.settings.preview_url;

        b.find(".pb_virtual_dom").remove();

        $.ajax({
        type: "POST",
        url: page_builder.settings.save_url,
        data: send,
        success: function(ret){

            delete page_builder.settings.document.html;

            if( typeof page_builder.settings.callback_after_save == "function" )
              page_builder.settings.callback_after_save( ret, b, page_builder );


        },

        error: function(ret){
          delete page_builder.settings.document.html;
          console.log(ret);
        },
        dataType: "json"
      });

    };

  };




};
