var page_menu_builder = function(b, page_builder) {

  var my_class = this;
  my_class.once = [];

  var code = new page_builder_codeview( b, page_builder );

  this.start = function() {


    this.getJson();

  };

  this.getMoveHandler = function(){

    b.find(".pb_mover").unbind("click");

    b.find(".pb_mover").click(function(){

      var li = $(this).closest("li");

      if( !li.prev("li").find("ul").length )
        return false;

      li.prev("li").find("ul").append( li.clone() );

      li.remove();

      my_class.getNewOrder();
      my_class.getAddMenuItem();
      my_class.getSaveMenu();

      return false;
    });

  };

  this.getMenuOrder = function(val) {

    my_class.getMoveHandler();


    var drop = new Draggable.Sortable(document.querySelectorAll('.pb_menu_editor ul'), {
      draggable: 'li[data-menu]',
      dropzone: '.pb_menu_editor ul',
      handle: '.pb_handle'

    }).on('drag:stop', function(ev) {

        setTimeout(function(){
          my_class.getNewOrder();
          my_class.getAddMenuItem();
          my_class.getSaveMenu();
          }, 500);
    });



  };

  this.getNewOrder = function(){


    my_class.once = [];
    var val = my_class.getNewVal(b.find(".pb_menu_editor"), {}, false);
    my_class.menu[ b.find(".pb_menu_select").val() ][ b.find(".pb_lang_select").val() ] = val;


    b.find(".pb_menu_editor").remove();


    b.find(".pb_lang_select").after('<div class="pb_menu_editor">' + my_class.getMenu(val, "") + '</div>');

    my_class.getMenuOrder(val);


  };

  this.getNewVal = function(row, val, second){



    row.find("ul li").each(function(){

      var txt = $(this).find(".pb_title").eq(0).text().trim();

      var test = $.inArray(txt, my_class.once );


      if( test != -1 )
        return true;

        if( $(this).find("ul li").length > 0  )
          val[ txt ] = my_class.getNewVal($(this), {});


        else
          val[ txt ] = $(this).attr("data-menu");

        my_class.once.push( txt );

    });

    return val;

  };

  this.getMenu = function(menu, html, parent) {

    if (typeof menu == "undefined" || !menu)
      return html;


    html += '<ul>';
    var add_html = "";
    $.each(menu, function(k, v) {

      add_html = "<ul></ul>";

      if (typeof v != "string")
        add_html = my_class.getMenu(v, "", true);

      html += my_class.getLi(v,k,add_html);

    });

    html += '</ul>';

    return html;


  };

  this.getLi = function(v, k, add_html){

    if(typeof v != "string")
      v = "";

    return '<li data-menu="' + v + '"><span class="pb_handle">•••</span><span class="pb_title">' + k + '</span><span class="pb_edit">edit</span><span class="pb_delete"><i class="pb_font pb_font-trash"></i></span><span class="pb_mover"><i class="pb_font pb_font-redo"></i></span> '+add_html+'</li>';
  };

  this.getSecondSelect = function(key) {

    if (b.find(".pb_lang_select"))
      b.find(".pb_lang_select").remove();


    if (typeof key == "undefined" || !key)
      return false;

    var html = "<select class='pb_lang_select'>";

    html += '<option value="">- Bitte auswählen -</option>';

    $.each(my_class.menu[key], function(k, v) {

      html += '<option value="' + k + '">' + k + '</option>';

    });

    html += '</select>';

    b.find(".pb_menu_select").after(html);

    b.find(".pb_lang_select").change(function() {

      b.find(".pb_menu_editor, .pb_add_item").remove();

      if (!$(this).val())
        return false;

      b.find(".pb_lang_select").after('<div class="pb_menu_editor">' + my_class.getMenu(my_class.menu[key][$(this).val()], "") + '</div><div class="pb_add_item">+</div>');

      my_class.getMenuOrder(my_class.menu[key][$(this).val()]);

      my_class.getAddMenuItem();
    });

  };

  this.getFirstSelect = function() {

    var html = "<select class='pb_menu_select'>";

    html += '<option value="">- Bitte auswählen -</option>';

    $.each(my_class.menu, function(k, v) {

      html += '<option value="' + k + '">' + k + '</option>';

    });

    html += '</select>';

    b.html('<div class="pb_menu_designer"><a href="#" class="pb_back">zurück</a>'+html+'</div>');

    b.find(".pb_menu_select").change(function() {

      var val = $(this).val();

      my_class.getSecondSelect(val);

    });

    my_class.getClickButtonBack();
  };

  this.getClickButtonBack = function(){

    b.find(".pb_menu_designer .pb_back").unbind("click");

    b.find(".pb_menu_designer .pb_back").click(function(){

      b.html("");

      b.page_manager(page_builder.settings);

        return false;
    });



  };

  this.getRemoveMenuItem = function(){

    b.find(".pb_delete").unbind("click");
    b.find(".pb_delete").click(function(){

      $(this).closest("li").fadeOut(1000, function(){ $(this).remove(); my_class.getNewOrder();
      my_class.getAddMenuItem();
      my_class.getMoveHandler();
      my_class.getSaveMenu(); });


    });

  };

  this.getAddMenuItem = function(){

    my_class.getRemoveMenuItem();

    b.find(".pb_add_item, .pb_edit").unbind("click");

    b.find(".pb_add_item, .pb_edit").click(function(){

      var item = false;

      if( $(this).hasClass("pb_edit") )
        item = $(this).closest("li");

      code.showMenuItem( item, function(v, k){

        if( item )
          item.replaceWith( my_class.getLi(v,k,'<ul></ul>') );

        else
          b.find(".pb_menu_editor ul").eq(0).append(
            my_class.getLi(v,k,'<ul></ul>')
          );

        my_class.getNewOrder();
        my_class.getAddMenuItem();
        my_class.getMoveHandler();
        my_class.getSaveMenu();

       });

      return false;
    });

  };

  this.getSaveMenu = function(){

    if( typeof page_builder.settings.menu_builder == "undefined" )
      return false;

    if( typeof page_builder.settings.menu_builder.save == "undefined" )
      return false;


    $.ajax({
      type: "POST",
      url: page_builder.settings.menu_builder.save,
      data: my_class.menu,
      success: function(ret) {

        console.log(ret);

      },
      dataType: "html"
    });

  };

  this.getJson = function() {

    if( typeof page_builder.settings.menu_builder == "undefined" )
      return false;

    if( typeof page_builder.settings.menu_builder.load == "undefined" )
      return false;

    $(".pb_manager_container").remove();

    $.ajax({
      type: "GET",
      url: page_builder.settings.menu_builder.load,
      data: {},
      success: function(ret) {
        my_class.menu = ret;
        my_class.getFirstSelect();
      },
      dataType: "json"
    });

  };

};
